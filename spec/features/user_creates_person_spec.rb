require "rails_helper"

feature "user edits existing person" do
  scenario "with valid data" do
    person = Person.create!(first_name: "Josh")

    visit person_path(person)
  	click_link "Edit"
  	fill_in "person_first_name", with: "New_Josh"
 	click_button "Update"

    expect(page).to have_content("New_Josh")
  end

  scenario "with invalid data" do
    person = Person.create!(first_name: "Joe")

 	visit person_path(person)
  	click_link "Edit"
  	fill_in "person_first_name", with: " "
 	click_button "Update"

    expect(page).to have_content("First name can't be blank")
  end
end

